package vergaderingsignage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Date;
import java.util.List;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.exception.service.local.ServiceLocalException;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.Mailbox;
import microsoft.exchange.webservices.data.search.CalendarView;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Haal kalendermomenten op en sorteer die op start tijd
 *
 * @author Leonardo Malik
 * @version 1.8.1
 */
public class VergaderingSignage {

    //-----------------------------------------------------------
    // Configuratie
    //-----------------------------------------------------------
    private static final String LOGIN_ADRES = "";
    private static final String LOGIN_WACHTWOORD = "";

    /**
     * Deze variable bevat alle vergaderruimtes. Veranderingen komen in deze variable en worden
     * automatisch toegepast. Full access voor loginAdres tot de kalenders van die vergaderruimtes is
     * wel nodig!
     */
    private static final List<String> VERGADERRUIMTES = asList("");

    /**
     * Naam van het bestand, standaardlocatie is cwd
     * Locatie wordt at runtime ingesteld met --locatie
     */
    private static final String BESTANDSNAAM = "data_vergaderingen.json";

    //-----------------------------------------------------------
    // Vanaf hier mag niet meer aangepast worden!
    //-----------------------------------------------------------
    private static ExchangeService service;

    private static ArrayList<Appointment> alleAfsprakenGesorteerd = new ArrayList<>();
    private static ZonedDateTime startDatum;
    private static ZonedDateTime eindDatum;
    private static boolean debugging;
    private static Path locatie = null;
    // Voor debugging
    private static int dag = 0, maand = 0, jaar = 0;
    private static boolean gebruikAangepasteDatum = false;
    // In cron modus tonen we niet het statusbericht "OK"
    private static boolean isInCronModus = false;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try {
            // Controleer de meegegeven argumenten
            if (args.length != 0)
                argumentenInstellen(args);

            debugBericht("BEGIN PROGRAMMA");

            // 365 login details registreren
            registreer365Details();

            // Data instellen
            ZonedDateTime datumVandaag = ZonedDateTime.now(ZoneId.of("CET"));
            //DateTime datumVandaag = new DateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("CET")));

            if (gebruikAangepasteDatum)
                startDatum = datumVandaag.withDayOfMonth(dag).withMonth(maand).withYear(jaar).withHour(8).withMinute(0);
                //startDatum = datumVandaag.withDate(jaar, maand, dag).withHourOfDay(8).withMinuteOfHour(0);
            else
                startDatum = datumVandaag.withHour(8).withMinute(0);
            //startDatum = datumVandaag.withHourOfDay(8).withMinuteOfHour(0);

            //eindDatum = startDatum.withHourOfDay(21).withMinuteOfHour(0);
            eindDatum = startDatum.withHour(21).withMinute(0);

            // Laad de vergaderingen van 365 en sorteer deze in een mooie array
            laadAlleVergaderingenEnSorteer();

            // en nu Json naar bestand schrijven
            schrijfNaarBestand(converteerNaarJson());

            // Geef statusbericht als alles oké was, maar niet in cron modus, want dat zorgt voor email spam
            if (!isInCronModus)
                System.out.println("OK");
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     *
     */
    private static void argumentenInstellen(String[] args)
    {
        for (int n = 0; n < args.length; n++) {
            switch (args[n]) {
                case "--debug":
                    activeerDebugging();
                    break;

                // Datum in dit formaat: YYYYMMDD, bv: 20170225
                case "--metdatum":
                    if (args[n+1] == null)
                        throw new NullPointerException("Er is geen datum meegegeven met --metdatum");
                    else if (args[n + 1].length() != 8)
                        throw new NullPointerException("Er is een foute datum meegegeven, gebruik dit formaat YYYYMMDD");

                    jaar = Integer.parseInt(args[n + 1].substring(0, 4));
                    maand = Integer.parseInt(args[n + 1].substring(4, 6));
                    dag = Integer.parseInt(args[n + 1].substring(6, 8));
                    gebruikAangepasteDatum = true;
                    break;

                // Waar moet het json bestand opgeslagen worden?
                case "--locatie":
                    if (args[n + 1] == null)
                        throw new NullPointerException("Er is geen locatie meegegeven met --locatie");
                    else {
                        Path pad = Paths.get(args[n + 1]);

                        if (Files.notExists(pad))
                            throw new NullPointerException("De opgegeven locatie bestaat niet of kan niet geopend worden");
                        else if (!Files.isDirectory(pad))
                            throw new NullPointerException("De opgegeven locatie is geen folder");

                        locatie = Paths.get(args[n + 1], BESTANDSNAAM);
                    }
                    break;

                case "--cron":
                    isInCronModus = true;
                    break;
            }
        }
    }

    /**
     * Gewoon 365 service aanmaken en URL instellen
     */
    private static void registreer365Details() throws URISyntaxException
    {
        debugBericht("REGISTREET 365 GEGEVENS");
        service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        ExchangeCredentials credentials = new WebCredentials(LOGIN_ADRES, LOGIN_WACHTWOORD);
        service.setCredentials(credentials);
        // Autdiscover versneller omdat we puur met 365 verbinden
        service.setEnableScpLookup(false);
        debugBericht("REGISTRATIE 365 GEGEVENS COMPLEET");

        debugBericht("BEGIN AUTODISCOVER");
        service.setUrl(new URI("https://outlook.office365.com/EWS/Exchange.asmx"));
        debugBericht("EINDE AUTODISCOVER");
    }

    /**
     * Haal alle kalenders op en voeg deze toe aan één array, zodat we deze juist kunnen sorteren
     */
    private static boolean laadAlleVergaderingenEnSorteer() throws Exception
    {
        ArrayList<Appointment> alleAfspraken = new ArrayList<>();

        CalendarView cView = new CalendarView(Date.from(startDatum.toInstant()), Date.from(eindDatum.toInstant()));

        for (String mailAdres : VERGADERRUIMTES) {
            debugBericht("\nStart laden: " + mailAdres);

            alleAfspraken.addAll(service.findAppointments(new FolderId(WellKnownFolderName.Calendar,
                    Mailbox.getMailboxFromString(mailAdres)), cView).getItems());

            debugBericht("Toegevoegd aan array: " + mailAdres);
        }

        // Werkt niet:
        /*Collections.sort(alleAfspraken, new Comparator<Appointment>() {
                            public int compare(Appointment a1, Appointment a2) {
                                    return a1.getStart().compareTo(a2.getStart());
                            }
                    });*/
        Appointment vroegsteAfspraak = null;
        Appointment laatsteAfspraak = null;
        // Moet de registratie van de vroegste en laatste afspraak gedaan worden?
        // Als deze al ingesteld zijn, wordt deze = true
        boolean skipRegistratie = false;

        debugBericht(String.format("\nBEGIN MET SORTEREN: %d afspraken\n", alleAfspraken.size()));

        if (alleAfspraken.isEmpty()) {
            debugBericht("Sorteren vroegtijdig gestopt omdat er geen afspraken zijn om te sorteren");
            return false;
        }

        //---
        // Sorteren volgens starttijd
        //
        // Eigen oplossing omdat ik noch met ArrayList sort noch stream sorted de
        // resultaten kon sorteren, zonder een exception te veroorzaken
        //---
        for (int i = 0; i < alleAfspraken.size(); i++) {
            if (!skipRegistratie) {
                // Als er maar een item in de lijst, gewoon bijhouden en stoppen met loop
                if (alleAfspraken.size() == 1) {
                    // We stellen laatsteAfspraak in omdat deze op het einde nog wordt toegevoegd aan
                    // alleAfsprakenGesorteerd
                    debugBericht("Er is maar 1 item in array alleAfspraken");
                    laatsteAfspraak = alleAfspraken.get(0);
                    break;
                }
                // Als er twee items in de lijst zijn, sorteren volgens vroegste en laatste en stoppen met loop
                else if (alleAfspraken.size() == 2) {
                    if (alleAfspraken.get(0).getStart().before(alleAfspraken.get(1).getStart())) {
                        vroegsteAfspraak = alleAfspraken.get(0);
                        laatsteAfspraak = alleAfspraken.get(1);
                        debugBericht("Er zijn twee items in array alleAfspraken en 0 before 1");

                        // vroegsteAfspraak moet al toegevoegd worden
                        alleAfsprakenGesorteerd.add(vroegsteAfspraak);
                    }
                    else {
                        vroegsteAfspraak = alleAfspraken.get(1);
                        laatsteAfspraak = alleAfspraken.get(0);
                        debugBericht("Er zijn twee items in array alleAfspraken en 1 before 0");

                        // vroegsteAfspraak moet al toegevoegd worden
                        alleAfsprakenGesorteerd.add(vroegsteAfspraak);
                    }
                    break;
                }

                // Er zijn meer dan twee items in de lijst, nu moeten we uitgebreider te werk gaan
                Date tijdVolgendeAfspraak = null;
                Date tijdHuidigeAfspraak = alleAfspraken.get(i).getStart();
                boolean stelVroegsteAfspraakIn = false;
                boolean stelLaatsteAfspraakIn = false;
                debugBericht(String.format("tijdHuidigeAfspraak: %s", tijdHuidigeAfspraak));

                // Is er wel nog eentje hierna?
                if (alleAfspraken.size() > i + 1) {
                    tijdVolgendeAfspraak = alleAfspraken.get(i + 1).getStart();
                    debugBericht(String.format("tijdVolgendeAfspraak: %s", tijdVolgendeAfspraak));
                }

                // We registreren de vroegste afspraak in de vroegsteAfspraak variable.
                // Na de eerste loop moet de dus een vroegsteAfspraak ingesteld worden
                if (vroegsteAfspraak == null) {
                    if (tijdVolgendeAfspraak == null || tijdHuidigeAfspraak.before(tijdVolgendeAfspraak) || tijdHuidigeAfspraak.equals(tijdVolgendeAfspraak))
                        vroegsteAfspraak = alleAfspraken.get(i);
                    else
                        vroegsteAfspraak = alleAfspraken.get(i + 1);

                    stelVroegsteAfspraakIn = true;
                }
                else if (tijdHuidigeAfspraak.before(vroegsteAfspraak.getStart())) {
                    vroegsteAfspraak = alleAfspraken.get(i);
                    stelVroegsteAfspraakIn = true;
                }

                // Zelfde gedachte als hierboven, maar dan met de laatste meeting
                if (laatsteAfspraak == null) {
                    if (tijdVolgendeAfspraak == null || tijdHuidigeAfspraak.after(tijdVolgendeAfspraak))
                        laatsteAfspraak = alleAfspraken.get(i);
                    else
                        laatsteAfspraak = alleAfspraken.get(i + 1);

                    stelLaatsteAfspraakIn = true;
                }
                else if (tijdHuidigeAfspraak.after(laatsteAfspraak.getStart())) {
                    laatsteAfspraak = alleAfspraken.get(i);
                    stelLaatsteAfspraakIn = true;
                }

                // Debugging
                if (stelVroegsteAfspraakIn) {
                    debugBericht("VROEGSTE afspraak geüpdatet naar: "+ tijdHuidigeAfspraak);
                    debugBericht("-> Vergeleken met: "+ tijdVolgendeAfspraak + "");
                }
                else if (stelLaatsteAfspraakIn) {
                    debugBericht("LAATSTE afspraak geüpdatet naar: "+ tijdHuidigeAfspraak);
                    debugBericht("-> Vergeleken met: "+ tijdVolgendeAfspraak + "");
                }

                debugBericht("- Registratie eerste/laatste afspraak gepasseerd...; ronde " + i + "\n");

                // de vroegste en laatste meetings zijn juist geregistreerd, nu de loop resetten
                if (alleAfspraken.size() == i + 1) {
                    i = -1; // i = 0 zorgde dat i vanaf 1 begint
                    skipRegistratie = true;
                    debugBericht("Vanaf nu wordt de registratie van vroegste en laatste afspraak geskipt");
                    // Deze mogen al verwijderd worden
                    alleAfspraken.remove(vroegsteAfspraak);
                    alleAfspraken.remove(laatsteAfspraak);
                    // De lijst moet uiteraard met de vroegste beginnen, zodat we kunnen sorteren
                    // met deze als referentie
                    alleAfsprakenGesorteerd.add(vroegsteAfspraak);
                }
            }
            // Dit wordt pas uitgevoerd nadat beide tijden geregistreerd zijn en dus skipRegistratie == true
            // Vanaf hier beginnen we de rest te sorteren
            // Na succesvol toevoegen break; om verder te gaan met loopen in alleAfspraken
            else {
                debugBericht("Registratie van eerste en laatste werd geskipt");
                for (int n = 0; n < alleAfsprakenGesorteerd.size(); n++) {
                    // Als de huidige meeting in alleAfsprakenGesorteerd == alleAfspraken.get(i),
                    // dan mag deze toegevoegd worden in de gesorteerde array. Want zijn beiden
                    // bijvoorbeeld 11:00 en 11:00 en mogen dus na mekaar komen.
                    if (alleAfspraken.get(i).getStart().equals(alleAfsprakenGesorteerd.get(n).getStart())) {
                        alleAfsprakenGesorteerd.add(n + 1, alleAfspraken.get(i));
                        debugBericht("Equals dus erna toegevoegd: " + alleAfspraken.get(i).getStart() + " toegevoegd aan index " + n + 1);
                        break;
                    }
                    // Als ze niet == zijn, dan moet er gecontroleerd worden of deze vroeger is dan
                    // de volgende in alleAfsprakenGesorteerd (.get(n+1)). Zo ja, mag die ertussen worden
                    // gezet, anders gewoon verder zoeken in de lijst
                    else {
                        // Deze valt tussen (after) .get(n) en (before) .get(n+1), dus mag ertussen komen
                        // Check of n+1 bestaat met size() <= n+1
                        // De rest wordt opgeschuifd
                        if ((alleAfsprakenGesorteerd.size() > n + 1)
                                && (alleAfspraken.get(i).getStart().before(alleAfsprakenGesorteerd.get(n + 1).getStart())
                                || alleAfspraken.get(i).getStart().equals(alleAfsprakenGesorteerd.get(n + 1).getStart())))
                        {
                            alleAfsprakenGesorteerd.add(n + 1, alleAfspraken.get(i));
                            debugBericht("Ertussen toegevoegd: " + alleAfspraken.get(i).getStart() + " toegevoegd aan index " + n + 1);
                            break;

                        }
                        // Het valt niet tussen de voorlaatste en de laatste, dus voeg het bij aan
                        // op het einde, maar we moeten wel zeker zijn dat we op het einde van de ArrayList zijn.
                        // Zo zijn we zeker dat we met alle waardes hebben vergeleken
                        else if ((alleAfsprakenGesorteerd.size() == n + 1)) {
                            alleAfsprakenGesorteerd.add(alleAfspraken.get(i));
                            debugBericht("Op het einde toegevoegd: " + alleAfspraken.get(i).getStart() + " toegevoegd aan index " + n + 1);
                            break;
                        }
                    }
                }
            }
        }
        debugBericht("Einde met lijst sorteren");

        // Ook nog de laatste toevoegen
        alleAfsprakenGesorteerd.add(laatsteAfspraak);

        debugBericht("Alleafspraken gesorteerd is zo groot: " + alleAfsprakenGesorteerd.size());

        return true;
    }

    /**
     * Methode om manueel JSON aan te maken, want Json, Gson en al dienen andere zever werkt niet
     * zonder gedoe met alleAfsprakenGesorteerd...
     *
     */
    private static String converteerNaarJson() throws ServiceLocalException
    {
        String jsonString = "{\"afspraken\":[";

        /*for (Appointment afs : alleAfsprakenGesorteerd) {
                            String onderwerp = afs.getSubject().substring(0, 1).toUpperCase() + afs.getSubject().substring(1);
                            Date start = afs.getStart();
                            Date einde = afs.getEnd();
                            String locatie = afs.getLocation();

                            System.out.println(onderwerp + ", Start: " + start + ", Einde: " + einde +
                                    ", Locatie: " + locatie);
                    }*/
        debugBericht("Json aan het maken");

        for (Appointment afs : alleAfsprakenGesorteerd) {
            jsonString += "{\"onderwerp\":\"" + StringEscapeUtils.escapeJson(StringUtils.capitalize(afs.getSubject())) + "\","
                    + "\"start\":\"" + afs.getStart() + "\","
                    + "\"einde\":\"" + afs.getEnd() + "\","
                    + "\"locatie\":\"" + StringEscapeUtils.escapeJson(afs.getLocation()) + "\"},";
        }

        // Laatste komma verwijderen als er afspraken in de lijst zijn
        if (!alleAfsprakenGesorteerd.isEmpty())
            jsonString = jsonString.substring(0, jsonString.length() - 1);

        // en Json beëindigen
        jsonString += "]}";

        debugBericht("Json maken gedaan, resultaat:");
        debugBericht(jsonString);

        return jsonString;
    }

    /**
     *
     */
    private static void schrijfNaarBestand(String tekst) throws IOException
    {
        BufferedWriter writer;

        if (locatie == null) {
            debugBericht("Naar bestand '" + BESTANDSNAAM + "' aan het schrijven");
            writer = Files.newBufferedWriter(Paths.get(BESTANDSNAAM), Charset.forName("UTF-8"));
        }
        else {
            debugBericht("Naar locatie '" + locatie + "' aan het schrijven");
            writer = Files.newBufferedWriter(locatie, Charset.forName("UTF-8"));
        }

        writer.write(tekst);
        writer.close();
        debugBericht("Naar bestand aan het schrijven gedaan");
    }

    /**
     *
     */
    private static void activeerDebugging()
    {
        debugging = true;
    }

    /**
     * Toon het bericht als debugging geactiveerd is
     *
     */
    private static void debugBericht(String tekst)
    {
        if (debugging)
            System.out.println(tekst);
    }
}
